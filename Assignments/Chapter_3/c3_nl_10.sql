USE ap;
SELECT vendor_id,
       invoice_number,
       invoice_date,
       invoice_total
FROM invoices
WHERE invoice_date >= '2014-05-01' AND
      invoice_date <= '2014-06-30'
ORDER BY invoice_total DESC
LIMIT 10;