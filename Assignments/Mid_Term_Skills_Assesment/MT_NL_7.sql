USE bbmsc;
SELECT is_name, 
	   SUM(cost * quantity) AS billed_tot
FROM inventory_items ii
INNER JOIN item_orders i_o ON ii.ii_id = i_o.ii_id
INNER JOIN item_supplier i_s ON i_o.is_id = i_s.is_id
WHERE is_name IN(SELECT is_name FROM item_supplier WHERE is_name LIKE "Lake%")
GROUP BY is_name
