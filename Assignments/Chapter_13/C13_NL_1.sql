USE ap;
DROP PROCEDURE IF EXISTS ch13_1;
DELIMITER //;
CREATE PROCEDURE ch13_1()
BEGIN

	SELECT 'This is my first procedure' AS Hello_World;
    
END //;
CALL ch13_1();