USE ap;
INSERT INTO invoices_copy(invoice_number,
                          invoice_total,
                          invoice_date,
                          payment_date,
                          invoice_due_date,
                          payment_total,
                          credit_total,
                          terms_id,
                          vendor_id,
                          invoice_id)
VALUES
    ('P-029324', 345.60, '2014-08-03', NULL, '2014-08-13', 0.00, 0.00, 1, 125, 115),
    ('546789', 1139.45, '2014-08-03', NULL, '2014-09-03', 0.00, 0.00, 3, 126, 116),
    ('129-45777', 250.99, '2014-08-04', NULL, '2014-08-24', 0.00, 0.00, 2, 124, 117);
