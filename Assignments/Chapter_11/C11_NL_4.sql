USE donations;
SELECT d.donor_first, 
	   d.donor_last, 
       dn.donation_desc, 
       dn.donation_value, 
       dn.donation_date, 
       a.agency_name
FROM donation dn
INNER JOIN donor d  ON d.donor_id = dn.donor_id
INNER JOIN agency a ON dn.agency_id = a.agency_id
WHERE donation_desc LIKE 'CASH'
ORDER BY donor_last;