USE ap;

DROP TRIGGER IF EXISTS vendors_copy_before_update;
DELIMITER //

CREATE TRIGGER vendors_copy_before_update
BEFORE UPDATE ON vendors_copy
FOR EACH ROW
BEGIN
    SET NEW.vendor_address2 = UPPER(NEW.vendor_address2);
END //

UPDATE vendors_copy
SET vendor_address2 = 'na'
WHERE vendor_address2 IS NULL;
SELECT *
FROM vendors_copy;
