FROM mysql:8
WORKDIR /class_files
RUN apt-get update && apt-get install -y tree vim && rm -rf /var/lib/apt/lists/*
COPY create_databases.sql create_databases.sql
COPY setup.sh setup.sh
