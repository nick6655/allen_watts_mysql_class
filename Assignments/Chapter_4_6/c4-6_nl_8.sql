USE ap;
SELECT DISTINCT vendor_name,
       SUM(invoice_total) AS total_inv_amount,
       COUNT(invoice_number) AS inv_count
FROM invoices
NATURAL JOIN vendors
WHERE vendor_state = 'CA'
GROUP BY vendor_name
HAVING inv_count > 1 AND
       SUM(invoice_total - (payment_total + credit_total)) = 0
ORDER BY inv_count;