USE ap;
SELECT COUNT(DISTINCT vendor_id) AS total,
       'Active accounts'         AS status
FROM invoices
UNION
SELECT COUNT(vendors.vendor_id) AS total,
       'Inactive accounts' AS status
FROM invoices
RIGHT OUTER JOIN vendors ON invoices.vendor_id = vendors.vendor_id
WHERE invoices.vendor_id IS NULL;