USE ap;
SELECT DISTINCT vendor_id, invoice_date
FROM invoices
ORDER BY vendor_id DESC;