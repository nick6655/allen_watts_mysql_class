USE ap;
SELECT vendor_name,
       vendor_city,
       vendor_state,
       invoice_number,
       invoice_date,
       invoice_total
FROM vendors
NATURAL JOIN invoices
WHERE invoice_total > 500 AND
      invoice_date >= '2014-06-01' AND
      invoice_date <= '2014-06-30'
ORDER BY vendor_name,
         invoice_total DESC;