USE ap;
SELECT vendor_name,
       vendor_state,
       IF(vendor_state = 'CA', 'California Vendor', 'Non California Vendor') AS 'CA Vendor?',
       invoice_number,
       invoice_date,
       invoice_total,
       invoice_total - payment_total - credit_total AS bal_due,
       IF(invoice_total - payment_total - credit_total = 0, 'PAID', 'NOT PAID') AS pay_status
FROM vendors
NATURAL JOIN invoices
ORDER BY vendor_name;


