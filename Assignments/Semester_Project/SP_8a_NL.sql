DROP FUNCTION IF EXISTS get_lead_time;
DELIMITER //
CREATE FUNCTION get_lead_time(
    stock_action_id_param INT
)
RETURNS VARCHAR(60) DETERMINISTIC
BEGIN

    DECLARE lead_time VARCHAR(60);
    
    SELECT CONCAT(min_lead_time_in_months, '-', max_lead_time_in_months, ' Months') INTO lead_time
    FROM production_notes
    WHERE stock_action_id = stock_action_id_param;

    RETURN(lead_time);
    
END //
DELIMITER ;

USE mc_stocks;
SELECT get_lead_time(111);
