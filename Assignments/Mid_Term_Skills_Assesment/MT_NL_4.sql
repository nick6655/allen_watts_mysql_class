USE bbmsc;
SELECT item,
	   description,
       sku,
       SUM(quantity) AS tot_ordered
FROM inventory_items i_i
INNER JOIN item_orders i_o ON i_i.ii_id = i_o.ii_id
WHERE order_date BETWEEN '2016-04-01' AND '2016-05-31'
GROUP BY item, description, sku
HAVING tot_ordered > 1 AND COUNT(io_id) > 1;
