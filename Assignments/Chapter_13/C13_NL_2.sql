USE ap;
DROP PROCEDURE IF EXISTS ch13_2;
DELIMITER //;
CREATE PROCEDURE ch13_2()
BEGIN

	DECLARE sum_inv_tot DEC(9,2);
	DECLARE inv_count, vend_id INT;
    
    SET vend_id = 123;
    
    SELECT SUM(invoice_total), 
           COUNT(*)
	INTO sum_inv_tot, inv_count
    FROM invoices
    WHERE vendor_id = vend_id;
    
    SELECT CONCAT('$', sum_inv_tot) AS TotalInvoiceAmt, inv_count AS InvoiceCount;
    
END //;
CALL ch13_2();