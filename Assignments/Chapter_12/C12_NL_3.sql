use cf;
CREATE OR REPLACE VIEW service_subtotal AS 
SELECT pp.service_id,
       pp.service_date,
       pp.service_desc,
       SUM(pp.tot_parts_price) AS tot_parts_cost
FROM parts_pricing pp
INNER JOIN labor_pricing lp ON lp.service_id = pp.service_id
GROUP BY pp.service_id, pp.service_date, pp.service_desc;
SELECT * FROM service_subtotal;