USE ap;
SELECT vendor_name,
       UPPER(vendor_name),
       CONCAT(SUBSTRING(vendor_contact_first_name, 1, 1), '  ', vendor_contact_last_name) AS v_name,
       SUBSTRING(vendor_phone, -4, 4) AS v_phone
FROM vendors
WHERE vendor_phone IS NOT NULL
ORDER BY v_name;

