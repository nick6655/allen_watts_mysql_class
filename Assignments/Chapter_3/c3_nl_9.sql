USE ap;
SELECT vendor_name,
       vendor_city,
       vendor_state,
       vendor_phone
FROM vendors
WHERE vendor_state = 'CA' and
      vendor_phone IS NULL
ORDER BY vendor_state ASC;