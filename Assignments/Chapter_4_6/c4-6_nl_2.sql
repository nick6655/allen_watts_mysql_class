USE ap;
SELECT vendor_name,
       invoices.invoice_id,
       invoice_date,
       invoice_total,
       line_item_amount,
       account_description,
       vendor_state
FROM vendors, invoices, invoice_line_items, general_ledger_accounts
WHERE vendors.vendor_id = invoices.vendor_id AND
      invoices.invoice_id = invoice_line_items.invoice_id AND
      general_ledger_accounts.account_number = vendors.default_account_number AND
      vendor_state IN ('AZ', 'CA', 'NV')
ORDER BY vendors.vendor_name;