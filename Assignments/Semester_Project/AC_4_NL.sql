DROP FUNCTION IF EXISTS compare_lead_time;
DELIMITER //
CREATE FUNCTION compare_lead_time(
    stock_1_param VARCHAR(60),
    stock_2_param VARCHAR(60)
)
RETURNS VARCHAR(240) DETERMINISTIC
BEGIN

    DECLARE comp_lead_time VARCHAR(240);
    DECLARE first_lead_time INT;
    DECLARE second_lead_time INT;
    
    SELECT AVG((min_lead_time_in_months + max_lead_time_in_months) / 2) AS avg_lead_time
    INTO first_lead_time
    FROM production_notes pn
    NATURAL JOIN stock_action_fitment saf
    NATURAL JOIN stocks s
    NATURAL JOIN actions a
    WHERE UPPER(stock_name) = UPPER(stock_1_param);
    
    SELECT AVG((min_lead_time_in_months + max_lead_time_in_months) / 2) AS avg_lead_time
    INTO second_lead_time
    FROM production_notes pn
    NATURAL JOIN stock_action_fitment saf
    NATURAL JOIN stocks s
    NATURAL JOIN actions a
    WHERE UPPER(stock_name) = UPPER(stock_2_param);

    SET comp_lead_time = CONCAT(
        UPPER(stock_1_param),
        ': ', 
        first_lead_time, 
        ' | ', 
        UPPER(stock_2_param), 
        ': ', 
        second_lead_time,
        ' | ',
        'Delta: ',
        (first_lead_time - second_lead_time)
    );

	RETURN(comp_lead_time);
    
END //
DELIMITER ;

USE mc_stocks;
SELECT compare_lead_time('a5 sk', 'ADJ A-TH SK');