USE donations;
ALTER TABLE donor    ADD donor_phone    VARCHAR(15);
ALTER TABLE agency   ADD agency_website VARCHAR(40);
ALTER TABLE donation ADD pickup_req     BIT NOT NULL;
