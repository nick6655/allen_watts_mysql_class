USE ap;
DROP PROCEDURE IF EXISTS ch13_5_part_2;
DELIMITER //;
CREATE PROCEDURE ch13_5_part_2(inventory_id INT)
BEGIN

    -- Declare VARS
    DECLARE v_state VARCHAR(2) DEFAULT '';
    DECLARE v_city VARCHAR(30) DEFAULT '';
    DECLARE inv_id INT;

    -- Default inventory_id if NULL otherwise set as passed
    SET inventory_id = IFNULL(inventory_id, 15);
    SET inv_id = inventory_id;
    
    -- Set v_state and v_city vars
	SELECT vendor_state, vendor_city 
    INTO v_state, v_city
    FROM vendors v
    INNER JOIN invoices i ON v.vendor_id = i.vendor_id
    WHERE invoice_id = inv_id;
    
    -- Evaluate vendor display message
	CASE
        WHEN v_state = 'AZ' THEN 
            SELECT 'Arizona vendor';
		WHEN v_state = 'CA' THEN
		    IF v_city = 'Fresno' THEN
				SELECT 'Fresno California vendor';
			ELSEIF v_city = 'Oxnard' THEN
                SELECT 'LA Metro California vendor';
			ELSE
                SELECT 'California vendor';
			END IF;
		ELSE
            SELECT 'National vendor';
    END CASE;
    
END //;
CALL ch13_5_part_2(NULL);
CALL ch13_5_part_2(10);
CALL ch13_5_part_2(12);
CALL ch13_5_part_2(15);
CALL ch13_5_part_2(46);