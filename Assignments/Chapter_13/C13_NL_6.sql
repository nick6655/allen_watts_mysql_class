USE ap;
DROP PROCEDURE IF EXISTS ch13_6;
DELIMITER //;
CREATE PROCEDURE ch13_6()
BEGIN

    -- Declare VARS
    DECLARE position INT DEFAULT 8;
    DECLARE bin_numb INT DEFAULT 128;
    DECLARE output_var VARCHAR(200) DEFAULT '';
    
    WHILE position > 0 DO
        SET output_var = CONCAT(output_var, 'Position ', position, ' = ', bin_numb, ' | ');
        SET position = position - 1;
        SET bin_numb = bin_numb / 2;
    END WHILE;
    
    SELECT output_var AS '8_bit_binary_digit';
END //;
CALL ch13_6();