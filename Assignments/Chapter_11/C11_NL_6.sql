USE donations;
SELECT donor_first,
       donor_last,
       donation_desc,
       donation_value,
       donation_value - (donation_value * .1) as actual_value
FROM donation
NATURAL JOIN donor
WHERE pickup_req = 1;