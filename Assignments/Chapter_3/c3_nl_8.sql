USE ap;
SELECT vendor_name,
       vendor_address1,
       vendor_city,
       vendor_state,
       vendor_zip_code
FROM vendors
WHERE vendor_state in ('AZ', 'IL', 'OH') or
      vendor_city in ('Los Angeles', 'Boston', 'Philadelphia')
ORDER BY vendor_city DESC;