#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import csv

class create_insert_statements(object):
    
    def __init__(self, full_data_array):
        self.data = full_data_array
        self.stocks = list()
        self.actions = list()

    def get_stocks_insert_statement(self):
        
        stocks_insert_statement = list()

        stocks = list()
        for row in self.data:
            stocks.append(row[0])

        stocks = set(stocks)
        self.stocks = stocks

        stocks_insert_statement.append("INSERT INTO stocks(stock_id, stock_name)")
        stocks_insert_statement.append("VALUES")

        for index, stock in enumerate(list(stocks)):
            if index < len(stocks) - 1:
                stocks_insert_statement.append("\t({0}, '{1}'),".format(index + 1, stock))
            else:
                stocks_insert_statement.append("\t({0}, '{1}');".format(index + 1, stock))

        return "\n".join(stocks_insert_statement)

    def get_actions_insert_statement(self):
    
        actions_insert_statement = list()

        actions = list()
        for row in self.data:
            actions.append(row[1])

        actions = set(actions)
        self.actions = actions

        actions_insert_statement.append("INSERT INTO actions(action_id, action_name)")
        actions_insert_statement.append("VALUES")

        for index, action in enumerate(list(actions)):
            if index < len(actions) - 1:
                actions_insert_statement.append("\t({0}, '{1}'),".format(index + 1, action))
            else:
                actions_insert_statement.append("\t({0}, '{1}');".format(index + 1, action))

        return "\n".join(actions_insert_statement)

    def get_stock_action_fitment_insert_statement(self):
    
        stock_action_fitment_insert_statement = list()

        stock_action_fitment_insert_statement.append("INSERT INTO stock_action_fitment(stock_action_id, stock_id, action_id, drop_at_comb, drop_at_heel, drop_to_toe, forearm_width, grip_to_tip, butt_width, butt_flat, grip_width, over_all_length, length_of_pull_no_pad, max_action_diameter, max_barrel_diameter, barrel_contour, cast_off, toe_angle_in_degrees)")
        stock_action_fitment_insert_statement.append("VALUES")

        for index, row in enumerate(list(self.data)):
            if index < len(self.data) - 1:
                stock_action_fitment_insert_statement.append(
                    "\t({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, NULL, '{14}', '{15}', '{16}'),".format(
                        index + 1,
                        list(self.stocks).index(row[0]) + 1,
                        list(self.actions).index(row[1]) + 1,
                        row[5],
                        row[6],
                        row[7],
                        row[8],
                        row[9],
                        row[10],
                        row[11],
                        row[12],
                        row[13],
                        row[14],
                        row[15],
                        row[16],
                        row[17],
                        row[18]
                    )
                )
            else:
                stock_action_fitment_insert_statement.append(
                    "\t({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, NULL, '{14}', '{15}', '{16}');".format(
                        index + 1,
                        list(self.stocks).index(row[0]) + 1,
                        list(self.actions).index(row[1]) + 1,
                        row[5],
                        row[6],
                        row[7],
                        row[8],
                        row[9],
                        row[10],
                        row[11],
                        row[12],
                        row[13],
                        row[14],
                        row[15],
                        row[16],
                        row[17],
                        row[18]
                    )
                )

        return "\n".join(stock_action_fitment_insert_statement)

    def get_production_notes_insert_statement(self):

        production_notes_insert_statement = list()

        production_notes_insert_statement.append("INSERT INTO production_notes(production_notes_id, stock_action_id, notes, min_lead_time, max_lead_time)")
        production_notes_insert_statement.append("VALUES")

        for index, row in enumerate(list(self.data)):
            if index < len(self.data) - 1:
                production_notes_insert_statement.append(
                    "\t({0}, {1}, '{2}', {3}, {4}),".format(
                        index + 1,
                        index + 1,
                        row[2],
                        row[3],
                        row[4]
                    )   
                )
            else:
                production_notes_insert_statement.append(
                    "\t({0}, {1}, '{2}', {3}, {4});".format(
                        index + 1,
                        index + 1,
                        row[2],
                        row[3],
                        row[4]
                    )   
                ) 

        return "\n".join(production_notes_insert_statement)


class normalize_columns(object):
    
    def __init__(self, csv_input_file):

        csv_file = list()

        with open(str(csv_input_file), 'r') as csvfile:
            stocks_table = csv.reader(csvfile, delimiter=',', quotechar='"')
            for row in stocks_table:
                csv_file.append(row)

        self.csv_file = csv_file

    def format_string(self, value):
        value = value.strip()
        value = value.replace("'", "\\\'")
        value = value.replace("\"", "\\\"")

        return value

    def format_columns(self):

        formatted_data = list()
        for index, row in enumerate(self.csv_file):
            
            if index == 0:
                continue
            
            formatted_row = list()

            formatted_row.append(self.format_string(str(row[0])))
            formatted_row.append(self.format_string(str(row[1])))
            formatted_row.append(self.format_string(str(row[2])))

            if 'months'.upper() in row[3].upper():
                formatted_row.append(row[3].strip().split()[0].split('-')[0])
                formatted_row.append(row[3].strip().split()[0].split('-')[1])
            else:
                formatted_row.append('NULL')
                formatted_row.append('NULL')

            try: 
                formatted_row.append(float(row[4]))
            except:
                formatted_row.append('NULL')

            try: 
                formatted_row.append(float(row[5]))
            except:
                formatted_row.append('NULL')

            try: 
                formatted_row.append(float(row[6]))
            except:
                formatted_row.append('NULL')

            try: 
                formatted_row.append(float(row[7]))
            except:
                formatted_row.append('NULL')

            try: 
                formatted_row.append(float(row[8]))
            except:
                formatted_row.append('NULL')

            try: 
                formatted_row.append(float(row[9]))
            except:
                formatted_row.append('NULL')

            try: 
                formatted_row.append(float(row[10]))
            except:
                formatted_row.append('NULL')

            try: 
                formatted_row.append(float(row[11]))
            except:
                formatted_row.append('NULL')

            try: 
                formatted_row.append(float(row[12]))
            except:
                formatted_row.append('NULL')

            try: 
                formatted_row.append(float(row[13]))
            except:
                formatted_row.append('NULL')

            try: 
                formatted_row.append(float(row[14]))
            except:
                formatted_row.append('NULL')

            formatted_row.append(self.format_string(str(row[15])))
            formatted_row.append(self.format_string(str(row[16])))
            formatted_row.append(self.format_string(re.sub("[^0-9]", "", str(row[17]))))
            
            formatted_data.append(formatted_row)

        return formatted_data 

if __name__ == '__main__':
    csv_file = '/Users/nicholaslong/Desktop/copy_stock_bible.csv'
    csv = normalize_columns(csv_file).format_columns()

    insert_statements = create_insert_statements(csv)
    print(insert_statements.get_stocks_insert_statement())
    print("")
    print(insert_statements.get_actions_insert_statement())
    print("")
    print(insert_statements.get_stock_action_fitment_insert_statement())
    print("")
    print(insert_statements.get_production_notes_insert_statement())
