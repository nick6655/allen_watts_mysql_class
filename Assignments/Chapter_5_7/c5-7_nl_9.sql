USE ap;
SELECT vendor_name,
       (
        SELECT AVG(invoice_total - payment_total - credit_total)
        FROM invoices
        WHERE v.vendor_id = invoices.vendor_id
       ) as max_bal_due
FROM vendors v
ORDER BY max_bal_due DESC
LIMIT 3;

