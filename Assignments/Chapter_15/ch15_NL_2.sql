USE cf;
DROP PROCEDURE IF EXISTS add_parts;
DELIMITER //

CREATE PROCEDURE add_parts(
	IN part_id   INT,
    IN parts_qty INT
)
BEGIN

    DECLARE last_service_id_var INT;

	DECLARE sql_error TINYINT DEFAULT FALSE;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		SET sql_error = TRUE;

    SELECT MAX(service_id) INTO last_service_id_var FROM service;

	START TRANSACTION;
           
    INSERT INTO parts_service(parts_service_id, part_id, service_id, parts_qty)
    VALUES(DEFAULT,
		   part_id,
		   last_service_id_var,
		   parts_qty);
           
	IF sql_error = FALSE THEN
		COMMIT;
		SELECT 'Record was added!' AS message;
	ELSE
		ROLLBACK;
		SELECT 'The part id you entered does not exist' AS message;
	END IF;

END//

-- These fail
CALL add_parts(24, 5);
CALL add_parts(25, 1);
CALL add_parts(26, 6);

-- These work
CALL add_parts(8, 1);
CALL add_parts(15, 7);
CALL add_parts(19, 1);

SELECT * FROM parts_service;

