USE ap;
SELECT vendor_name,
       invoice_number,
       invoice_total,
       CASE
           WHEN invoice_total > 20000 THEN 'Largest Invoices'
           WHEN invoice_total BETWEEN 5001 AND 20000 THEN 'Large Invoices'
           WHEN invoice_total BETWEEN 500 AND 5000 THEN 'Medium Invoice'
           ELSE 'Small Invoices'
       END AS invoice_type
FROM vendors
NATURAL JOIN invoices
ORDER BY invoice_total DESC;


