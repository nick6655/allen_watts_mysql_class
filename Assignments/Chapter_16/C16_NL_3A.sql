USE ap;
DROP TABLE IF EXISTS invoice_audit;

CREATE TABLE invoice_audit(
    vendor_id      INT         NOT NULL,
    invoice_number VARCHAR(50) NOT NULL,
    invoice_total  DEC(9,2)    NOT NULL,
    action_type    VARCHAR(50) NOT NULL,
    action_date    DATETIME    NOT NULL
);
