CREATE OR REPLACE VIEW get_avg_ergo_details_for_rem_stocks AS
SELECT CONCAT(action_name, ' ', stock_name) AS stock,
       AVG(over_all_length) AS avg_oal,
       AVG(grip_width) AS avg_gw,
       AVG(length_of_pull_no_pad) AS avg_lop
FROM stock_action_fitment sa
INNER JOIN stocks s ON s.stock_id = sa.stock_id
INNER JOIN actions a ON a.action_id = sa.action_id
WHERE action_name LIKE "%REMINGTON%"
GROUP BY stock_name, action_name
HAVING AVG(over_all_length) IS NOT NULL AND
       AVG(grip_width) IS NOT NULL AND
       AVG(length_of_pull_no_pad) IS NOT NULL
ORDER BY stock;

USE mc_stocks;
SELECT * FROM get_avg_ergo_details_for_rem_stocks;