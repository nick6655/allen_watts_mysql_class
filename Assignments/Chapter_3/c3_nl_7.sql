USE ap;
SELECT vendor_id,
       vendor_name,
       vendor_city,
       vendor_state,
       vendor_phone
FROM vendors
WHERE vendor_name LIKE 'C%' or
      vendor_state='CA'
ORDER BY vendor_city ASC;