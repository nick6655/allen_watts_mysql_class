USE ap;
SELECT vendor_name,
       CONCAT(vendor_city, ', ', vendor_state),
       invoice_number,
       invoice_total
FROM vendors
LEFT OUTER JOIN invoices ON vendors.vendor_id = invoices.vendor_id
ORDER BY invoice_number;