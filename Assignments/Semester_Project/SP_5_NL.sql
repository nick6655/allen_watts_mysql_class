CREATE OR REPLACE VIEW get_lead_time_for_howa_1500_stocks AS
SELECT CONCAT(
    action_name, 
    ' ', 
    stock_name, 
    '\'s lead time is ', 
    min_lead_time_in_months, 
    '-', 
    max_lead_time_in_months, 
    ' months'
) AS howa_stocks_lead_time
FROM stock_action_fitment sa
INNER JOIN stocks s ON s.stock_id = sa.stock_id
INNER JOIN production_notes pn ON pn.stock_action_id = sa.stock_action_id
INNER JOIN actions a ON a.action_id = sa.action_id
WHERE sa.stock_action_id IN (
    SELECT sa.stock_action_id
    FROM stock_action_fitment sa
    INNER JOIN production_notes pn ON pn.stock_action_id = sa.stock_action_id
    INNER JOIN actions a ON a.action_id = sa.action_id
    WHERE action_name = 'HOWA 1500' AND
          min_lead_time_in_months IS NOT NULL AND 
          max_lead_time_in_months IS NOT NULL
);

USE mc_stocks;
SELECT * FROM get_lead_time_for_howa_1500_stocks;