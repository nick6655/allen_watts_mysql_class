USE ap;

DROP TRIGGER IF EXISTS invoices_copy_before_update;
DELIMITER //

CREATE TRIGGER invoices_copy_before_update
BEFORE UPDATE ON invoices_copy
FOR EACH ROW
BEGIN
    DECLARE pay_tot DEC(9,2);
    
    SELECT payment_total INTO pay_tot FROM invoices_copy;
    
    IF pay_tot > 0 THEN
		SIGNAL SQLSTATE 'HY000'
        SET MESSAGE_TEXT = 'The invoice has already been paid- updates to the invoice total column are prohibited!';
	END IF;

END //

UPDATE invoices_copy
SET invoice_total = invoice_total + 100
WHERE invoice_id = 2;

UPDATE invoices_copy
SET invoice_total = invoice_total + 100
WHERE invoice_id = 110;

SELECT * FROM invoices_copy
WHERE invoice_id = 110;