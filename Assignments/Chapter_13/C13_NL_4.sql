USE ap;
DROP PROCEDURE IF EXISTS ch13_4;
DELIMITER //;
CREATE PROCEDURE ch13_4()
BEGIN

    DECLARE tot_due DEC(9,2);

	SELECT SUM(invoice_total - payment_total - credit_total)
    INTO tot_due
	FROM invoices;

    IF tot_due > 50000 THEN 
		SELECT "High invoice balance detected" AS high_balance;
	ELSEIF tot_due BETWEEN 30000 AND 50000 THEN
        SELECT "Substantial invoice balance detected" AS substantial_balance;
	ELSEIF  tot_due BETWEEN 20000 AND 29999 THEN
        SELECT "Manageable invoice balance detected" AS manageable_balance;
	ELSE
        SELECT "Low invoice balance detected" AS low_balance;
	END IF;
    
END //;
CALL ch13_4();