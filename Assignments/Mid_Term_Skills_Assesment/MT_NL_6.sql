USE bbmsc;
SELECT is_name,
       item,
       sku,
       cost,
       quantity,
       order_date,
       cost * quantity AS billed_amt
FROM inventory_items ii
INNER JOIN item_orders i_o ON ii.ii_id = i_o.ii_id
INNER JOIN item_supplier i_s ON i_s.is_id = i_o.is_id
WHERE order_date <= '2016-04-30'
ORDER BY quantity, billed_amt DESC
       