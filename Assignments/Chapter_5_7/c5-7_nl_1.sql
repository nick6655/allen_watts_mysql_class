USE ap;
-- vendor_id, vendor_name, vendor_address1, vendor_address_2, vendor_city, vendor_state, vendor_zip_code, vendor_phone, vendor_contact_last_name, vendor_contact_first_name, default_terms_id, default_account_name
INSERT INTO vendors_copy VALUES
(124, 'Waste Management', '23 E. Washington', '', 'Phoenix', 'AZ', '85001', '(602) 279-5641', 'Reeves', 'Bryant', 2, 521),
(125, 'Zip Printing', '412 N. Elm', '', 'Bakersfield', 'CA', '92659', '(559) 871-6564', 'Smith', 'Kathy', 1, 551),
(126, 'ACM Development', '8974 S. 17th Ave', 'Suite 200', 'Seattle', 'WA', '98102', '(206) 541-9112', 'Ayres', 'Ernesto', 4, 167);
