use cf;
CREATE OR REPLACE VIEW service_billing AS
SELECT pp.service_id,
       fleet_make,
       fleet_model,
       pp.service_date,
       pp.service_desc,
       CAST(ss.tot_parts_cost AS DECIMAL(9,2)) AS tot_parts_cost,
       CAST(lp.labor_price AS DECIMAL(9,2)) AS labor_price,
       CAST((ss.tot_parts_cost + lp.labor_price) AS DECIMAL(9,2)) AS tot_billing
FROM parts_pricing pp
INNER JOIN labor_pricing lp ON lp.service_id = pp.service_id
INNER JOIN service_subtotal ss ON ss.service_id = pp.service_id
GROUP BY pp.service_id,
         fleet_make,
         fleet_model,
         pp.service_date,
         pp.service_desc,
         ss.tot_parts_cost, 
         lp.labor_price
ORDER BY service_id;
SELECT * FROM service_billing;