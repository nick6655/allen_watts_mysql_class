USE cf;
CREATE OR REPLACE VIEW parts_pricing AS 
SELECT s.service_id,
       fleet_make,
       fleet_model,
       service_date,
       service_desc,
       part_name,
       parts_qty,
       part_cost,
       parts_qty * part_cost AS tot_parts_price
FROM fleet f JOIN service s
ON f.fleet_id = s.fleet_id
JOIN parts_service ps ON s.service_id = ps.service_id
JOIN parts p ON ps.part_id = p.part_id;

SELECT * FROM parts_pricing;