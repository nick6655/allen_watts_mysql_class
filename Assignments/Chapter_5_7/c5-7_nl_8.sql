USE ap;
SELECT vendor_name,
       invoice_number,
       invoice_date,
       invoice_total
FROM vendors
INNER JOIN invoices i ON vendors.vendor_id = i.vendor_id
WHERE invoice_date > (
    SELECT MIN(invoice_date)
    FROM invoices
    WHERE invoices.vendor_id = i.vendor_id
)
ORDER BY vendor_name, invoice_date;
