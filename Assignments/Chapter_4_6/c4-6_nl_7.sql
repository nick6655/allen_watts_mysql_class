USE ap;
SELECT vendor_id,
       SUM(invoice_total) AS invoice_gt,
       AVG(invoice_total) AS invoice_avg,
       COUNT(invoice_total) AS invoice_qty
FROM vendors
NATURAL JOIN invoices
GROUP BY vendor_id
HAVING invoice_avg > 300
ORDER BY invoice_avg DESC;