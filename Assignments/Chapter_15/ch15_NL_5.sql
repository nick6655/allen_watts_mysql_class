USE ap;
DROP FUNCTION IF EXISTS get_invoice_total;
DELIMITER //

CREATE FUNCTION get_invoice_total(
	vendor_id_param INT
)
RETURNS DEC(9,2)
-- Added the two below lines to squash errors.
READS SQL DATA
DETERMINISTIC
BEGIN

	DECLARE invoices_total DEC(9,2);
    
	SELECT SUM(invoice_total) INTO invoices_total
	FROM invoices
	WHERE vendor_id = vendor_id_param;

	IF invoices_total IS NULL THEN
		SET invoices_total = 0;
	END IF;

	RETURN(invoices_total);
    
END//

SELECT get_invoice_total(122) AS total_invoices