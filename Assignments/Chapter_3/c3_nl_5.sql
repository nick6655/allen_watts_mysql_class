USE ap;
SELECT invoice_number,
       invoice_date,
       invoice_total,
       invoice_total - (payment_total + credit_total) as balance_due,
       invoice_total * (0.15) as late_fee
FROM invoices
WHERE invoice_total >= 300 AND
      invoice_total <= 3000
ORDER BY invoice_total DESC;