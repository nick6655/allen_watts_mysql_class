USE ap;
SELECT vendor_id,
       invoice_number,
       invoice_date,
       invoice_total,
       invoice_total - payment_total - credit_total AS balance_due
FROM invoices
WHERE vendor_id IN(
    SELECT vendor_id
    FROM vendors
    WHERE vendor_id IN(110,121,122,123)
)
ORDER BY vendor_id, balance_due, invoice_total;