USE cf;
CREATE VIEW inventory_update AS
SELECT p.part_id,
       part_name,
       part_inventory,
       SUM(parts_qty) AS qty_used,
       IF(SUM(parts_qty) IS NULL, part_inventory, part_inventory - SUM(parts_qty)) AS new_inventory
FROM parts p
LEFT OUTER JOIN parts_service ps ON p.part_id = ps.part_id
GROUP BY p.part_id, part_name, part_inventory
ORDER BY part_id;
SELECT * FROM inventory_update;
