USE ap;
DROP PROCEDURE IF EXISTS select_vendors;
DELIMITER //

CREATE PROCEDURE select_vendors(
    IN vend_state_param VARCHAR(2)
)
BEGIN

    DECLARE select_clause VARCHAR(200) DEFAULT '';
    DECLARE where_clause  VARCHAR(200) DEFAULT '';
    
    SET select_clause = "SELECT vendor_name, 
                                vendor_state, 
                                vendor_city
                         FROM vendors ";
    SET where_clause  = "WHERE ";
    
    IF vend_state_param IS NOT NULL THEN
        SET where_clause = CONCAT(where_clause, "vendor_state = '", vend_state_param, "'");
    END IF;

    IF where_clause = "WHERE " THEN
        SET @dynamic_sql = select_clause;
    ELSE 
        SET @dynamic_sql = CONCAT(select_clause, where_clause);
    END IF;

    PREPARE dyn_sql_statement
    FROM @dynamic_sql;
    EXECUTE dyn_sql_statement;
    DEALLOCATE PREPARE dyn_sql_statement;

END //

CALL select_vendors('CA');
