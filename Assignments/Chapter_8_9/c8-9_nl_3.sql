USE ap;
SELECT invoice_number,
       invoice_total,
       ROUND(invoice_total) AS inv_tot_rd,
       invoice_total - payment_total - credit_total AS bal_due,
       ROUND(invoice_total - payment_total - credit_total) AS bal_due_rd
FROM invoices
WHERE invoice_total - payment_total - credit_total > 0
ORDER BY vendor_id;


