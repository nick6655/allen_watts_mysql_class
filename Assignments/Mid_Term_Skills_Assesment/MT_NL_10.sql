use bbmsc;
SELECT item,
       cost,
       markup,
       item_qty,
       pos_date,
       pos_number,
       CAST(item_qty * cost AS DECIMAL(9,2)) AS tot_cost,
       CAST(item_qty * (cost + (cost * markup)) AS DECIMAL(9,2)) AS retail_tot,
       CAST((item_qty * (cost + (cost * markup))) - (item_qty * cost) AS DECIMAL(9,2)) AS profit
FROM item_pos ip
INNER JOIN inventory_items ii ON ii.ii_id = ip.ii_id
INNER JOIN pos p ON ip.p_id = p.p_id
ORDER BY pos_date, pos_number, profit DESC