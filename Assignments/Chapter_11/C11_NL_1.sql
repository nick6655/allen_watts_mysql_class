CREATE DATABASE donations;
USE donations;
CREATE TABLE donor(
    donor_id      INT         NOT NULL AUTO_INCREMENT,
    doner_first   VARCHAR(20) NOT NULL,
    doner_last    VARCHAR(25) NOT NULL,
    donor_address VARCHAR(40),
    donor_city    VARCHAR(20),
    donor_state   VARCHAR(2),
    donor_zip     VARCHAR(14),
    CONSTRAINT donor_pk PRIMARY KEY (donor_id)
);
CREATE TABLE agency(
    agency_id     INT         NOT NULL AUTO_INCREMENT,
    agency_name   VARCHAR(50) NOT NULL,
    contact_first VARCHAR(20) NOT NULL,
    contact_last  VARCHAR(25) NOT NULL,
    contact_phone VARCHAR(15),
    CONSTRAINT agency_pk PRIMARY KEY (agency_id)
);
CREATE TABLE donation(
    donation_id    INT          NOT NULL AUTO_INCREMENT,
    donation_value DECIMAL(9,2) NOT NULL,
    donation_desc  VARCHAR(100) NOT NULL,
    donation_date  DATE         NOT NULL,
    donor_id       INT,
    agency_id      INT,
    CONSTRAINT donation_pk        PRIMARY KEY (donation_id),
    CONSTRAINT donation_fk_donor  FOREIGN KEY (donor_id)  REFERENCES donor(donor_id),
    CONSTRAINT donation_fk_agency FOREIGN KEY (agency_id) REFERENCES agency(agency_id)
); 