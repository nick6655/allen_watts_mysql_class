USE bbmsc;
SELECT p.p_id,
       pos_customer,
       pos_payment,
       SUBSTRING_INDEX(pos_customer,' ',-1) AS l_name,
       SUM(CAST(item_qty * (cost + (cost * markup)) AS DECIMAL(9,2))) AS pos_total
FROM item_pos ip
INNER JOIN inventory_items ii ON ii.ii_id = ip.ii_id
INNER JOIN pos p ON ip.p_id = p.p_id
WHERE pos_payment = 'Visa'
GROUP BY p.p_id, pos_customer, pos_payment, l_name
ORDER BY l_name
