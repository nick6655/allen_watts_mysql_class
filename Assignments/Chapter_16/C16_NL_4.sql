USE ap;

DROP EVENT IF EXISTS add_audit_rows;
DELIMITER //

CREATE EVENT add_audit_rows
ON SCHEDULE EVERY 1 MINUTE
DO BEGIN
    INSERT INTO invoice_audit
    VALUES(999, 'Test Invoice', 0.00, 'Test Insert', NOW());
END //

SELECT * FROM invoice_audit;