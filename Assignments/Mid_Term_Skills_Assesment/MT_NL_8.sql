USE bbmsc; 
SELECT pos_date,
	   pos_number,
       pos_customer,
       pos_payment,
       item,
       item_qty,
       CAST(item_qty * (cost + (cost * markup)) AS DECIMAL(9,2)) AS sub_total
FROM item_pos ip
INNER JOIN inventory_items ii ON ii.ii_id = ip.ii_id
INNER JOIN pos p ON ip.p_id = p.p_id
ORDER BY pos_date, pos_number, sub_total DESC
