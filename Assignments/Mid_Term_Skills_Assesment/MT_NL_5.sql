USE bbmsc;
SELECT item, 
       description, 
       sku, 
       cost, 
       markup
FROM inventory_items
WHERE ii_id NOT IN(
    SELECT ii_id
    FROM item_orders
)
ORDER BY cost DESC