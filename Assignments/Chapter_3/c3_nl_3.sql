USE ap;
SELECT vendor_id      AS 'Vendor Num',
       invoice_number AS 'Invoice #',
       invoice_total  AS 'Total Amount'
FROM invoices
ORDER BY invoice_total  DESC,
         invoice_number ASC;