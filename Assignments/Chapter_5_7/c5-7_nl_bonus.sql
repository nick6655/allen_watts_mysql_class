USE ap;
SELECT vendor_name,
       (
        SELECT MAX(invoice_total)
        FROM invoices
        WHERE v.vendor_id = invoices.vendor_id
       ) AS max_total
FROM vendors v
HAVING max_total IS NOT NULL
ORDER BY max_total DESC;

