USE ap;
SELECT vendor_state,
       SUM(invoice_total) AS state_totals,
       AVG(invoice_total) AS state_avg,
       MAX(invoice_total) AS state_max,
       MIN(invoice_total) AS state_min,
       COUNT(invoice_total) AS state_inv_count
FROM vendors
NATURAL JOIN invoices
GROUP BY vendor_state
ORDER BY state_inv_count DESC;