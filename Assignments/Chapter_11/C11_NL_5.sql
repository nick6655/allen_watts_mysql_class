USE donations;
SELECT donor_first, 
       donor_last, 
       donor_phone
FROM donor
WHERE donor_id NOT IN(
    SELECT donor_id 
    FROM donation 
);
