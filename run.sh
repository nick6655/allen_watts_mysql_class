#!/bin/bash

docker-compose -f mysql_class.yml build
docker-compose -f mysql_class.yml up -d --force-recreate
sleep 20;
docker exec -it mysql_class_db ./setup.sh
