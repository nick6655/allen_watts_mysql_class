USE ap;

/*
-- This uses the CASE Function to shorten the below statements.
SELECT vendor_name,
       vendor_zip_code,
       CASE
           WHEN vendor_city = 'Fresno' THEN 'Fresno area'
           WHEN vendor_city IN('Los Angeles', 'Anaheim', 'Inglewood', 'Pasadena') THEN 'LA area'
           ELSE 'CA regional'
       END AS zone_location
FROM vendors
WHERE vendor_state = 'CA'
ORDER BY zone_location, vendor_name;
*/

SELECT vendor_name,
       vendor_zip_code,
       'Fresno area' as zone_location
FROM vendors
WHERE vendor_city = 'Fresno'
UNION
SELECT vendor_name,
       vendor_zip_code,
       'LA area' AS zone_location
FROM vendors
WHERE vendor_city IN('Los Angeles', 'Anaheim', 'Inglewood', 'Pasadena')
UNION
SELECT vendor_name,
       vendor_zip_code,
       'CA regional' AS zone_location
FROM vendors
WHERE vendor_state = 'CA' AND
      vendor_city != 'Fresno' AND
      vendor_city NOT IN('Los Angeles', 'Anaheim', 'Inglewood', 'Pasadena')
ORDER BY zone_location, vendor_name;