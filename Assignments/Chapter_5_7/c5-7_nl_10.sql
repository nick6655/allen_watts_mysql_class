USE ap;
SELECT vendor_name,
       (
        SELECT MAX(invoice_total)
        FROM invoices
        WHERE v.vendor_id = invoices.vendor_id
       ) AS max_total
FROM vendors v
ORDER BY max_total DESC;

