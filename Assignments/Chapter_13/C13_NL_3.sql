USE ap;
DROP PROCEDURE IF EXISTS ch13_3;
DELIMITER //;
CREATE PROCEDURE ch13_3()
BEGIN

    DECLARE first_date, second_date DATE;
    DECLARE inv_amount DEC(9,2);
    
    SET first_date = '2014-05-01';
    SET second_date = '2014-05-30';
    SET inv_amount = 150.00;
    
    SELECT vendor_name, 
           invoice_date, 
           invoice_number, 
           invoice_total
	FROM vendors v
    INNER JOIN invoices i ON v.vendor_id = i.vendor_id
    WHERE invoice_date BETWEEN first_date AND second_date AND
          invoice_total > inv_amount;
    
END //;
CALL ch13_3();