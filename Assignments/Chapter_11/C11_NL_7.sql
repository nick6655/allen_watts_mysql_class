USE donations;
SELECT donor_first,
       donor_last,
       SUM(donation_value) as total_donations,
       MAX(donation_value) as largest_donation,
       AVG(donation_value) as avg_donation
FROM donation
NATURAL JOIN donor
GROUP BY donor_first, donor_last WITH ROLLUP;