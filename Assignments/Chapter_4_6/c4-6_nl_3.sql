USE ap;
SELECT vendor_name,
       vendor_city,
       invoice_number,
       invoice_total,
       invoice_total - (payment_total + credit_total) AS balance_due,
       line_item_amount
FROM vendors
NATURAL JOIN invoices
NATURAL JOIN invoice_line_items
WHERE vendor_city = 'Fresno' AND
      (invoice_total - (payment_total + credit_total)) = 0 AND
      invoice_total >= 300 AND
      invoice_total <= 800
ORDER BY invoice_total DESC;