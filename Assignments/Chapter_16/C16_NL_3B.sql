USE ap;

DROP TRIGGER IF EXISTS invoices_copy_after_insert;
DELIMITER //

CREATE TRIGGER invoices_copy_after_insert
AFTER INSERT ON invoices_copy
FOR EACH ROW
BEGIN
    INSERT INTO invoice_audit
    VALUES(
        NEW.vendor_id, 
        NEW.invoice_number,
        NEW.invoice_total,
	    'INSERTED', 
        NOW()
    );
END //

DELIMITER ;

INSERT INTO invoices_copy VALUES
(115, 110, 'PO-345', '2015-11-16', 7549.86, 0, 0, 2, '2015-12-03', NULL),
(116, 34, 'GVA-090', '2015-11-17', 12511.50, 0, 0, 3, '2015-12-17', NULL),
(117, 34, 'GVA-091', '2015-11-17', 510.00, 0, 0, 3, '2015-12-17', NULL);