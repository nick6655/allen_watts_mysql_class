CREATE DATABASE BBMSC;
USE BBMSC;

CREATE TABLE item_supplier(
    is_id         INT         NOT NULL AUTO_INCREMENT,
    is_name       VARCHAR(40) NOT NULL,
    is_address1   VARCHAR(40) NOT NULL,
    is_address2   VARCHAR(40),
    is_city       VARCHAR(25) NOT NULL,
    is_state      VARCHAR(2)  NOT NULL,
    is_zip        VARCHAR(15) NOT NULL,
    is_phone      VARCHAR(15) NOT NULL,
    is_contact_fn VARCHAR(20) NOT NULL,
    is_contact_ln VARCHAR(25) NOT NULL,
    CONSTRAINT is_id_pk PRIMARY KEY (is_id)
);
CREATE TABLE inventory_items(
    ii_id       INT          NOT NULL AUTO_INCREMENT,
    item        VARCHAR(25)  NOT NULL,
    description VARCHAR(50)  NOT NULL,
    sku         VARCHAR(25)  NOT NULL,
    cost        DECIMAL(9,2) NOT NULL,
    markup      DECIMAL(9,2) NOT NULL,
    CONSTRAINT ii_id_pk PRIMARY KEY (ii_id)
);
CREATE TABLE pos(
    p_id         INT         NOT NULL AUTO_INCREMENT,
    pos_date     DATE        NOT NULL,
    pos_number   INT         NOT NULL,
    pos_customer VARCHAR(60) NOT NULL,
    pos_payment  VARCHAR(20) NOT NULL,
    CONSTRAINT p_id_pk PRIMARY KEY (p_id)
);
CREATE TABLE item_pos(
    ip_id    INT NOT NULL AUTO_INCREMENT,
    ii_id    INT NOT NULL,
    p_id     INT NOT NULL,
    item_qty INT NOT NULL,
    CONSTRAINT ip_id_pk  PRIMARY KEY (ip_id),
    CONSTRAINT ip_fk_ii  FOREIGN KEY (ii_id) REFERENCES inventory_items(ii_id),
    CONSTRAINT ip_fk_pos FOREIGN KEY (p_id)  REFERENCES pos(p_id)
);
CREATE TABLE item_orders(
    io_id      INT  NOT NULL AUTO_INCREMENT,
    ii_id      INT  NOT NULL,
    is_id      INT  NOT NULL,
    order_date DATE NOT NULL,
    quantity   INT  NOT NULL,
    rec_date   DATE,
    CONSTRAINT io_id_pk PRIMARY KEY (io_id),
    CONSTRAINT io_fk_ii FOREIGN KEY (ii_id) REFERENCES inventory_items(ii_id),
    CONSTRAINT io_fk_is FOREIGN KEY (is_id) REFERENCES item_supplier(is_id)
);
