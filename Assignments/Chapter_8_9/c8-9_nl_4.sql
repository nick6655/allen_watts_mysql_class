USE ap;
SELECT vendor_name,
       invoice_number,
       invoice_date,
       DATE_FORMAT(invoice_date, '%W, %M %D, %Y') AS inv_date_formatted,
       DATE_FORMAT(invoice_date, '%M') AS inv_month,
       DATE_FORMAT(invoice_date, '%Y') AS inv_year,
       DATE_FORMAT(invoice_date, '%W') AS inv_day,
       DATE_ADD(invoice_date, INTERVAL 2 DAY) AS inv_date_plus2,
       DATEDIFF('2016-01-01', invoice_date) AS inv_date_diff
FROM vendors
NATURAL JOIN invoices
WHERE invoice_date BETWEEN '2014-06-01' AND '2014-06-30';


