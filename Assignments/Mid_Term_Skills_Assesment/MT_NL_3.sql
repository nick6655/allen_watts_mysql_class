USE bbmsc;
SELECT item,
	   sku,
       cost,
       is_name,
       is_phone,
       CONCAT(is_contact_fn, ' ', is_contact_ln) as contact,
       is_address1,
       is_address2,
       is_city,
       is_state,
       is_zip
FROM inventory_items i_i
INNER JOIN item_orders i_o ON i_o.ii_id = i_i.ii_id
INNER JOIN item_supplier i_s ON i_s.is_id = i_o.is_id
ORDER BY is_name;
       