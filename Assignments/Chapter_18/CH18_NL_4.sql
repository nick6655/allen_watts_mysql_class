CREATE USER ap_invoice_clerk IDENTIFIED BY 'password';
GRANT SELECT ON ap.vendors TO ap_invoice_clerk;
GRANT SELECT ON ap.general_ledger_accounts TO ap_invoice_clerk;
GRANT SELECT, UPDATE, INSERT ON ap.invoices TO ap_invoice_clerk;
GRANT SELECT, UPDATE, INSERT ON ap.invoice_line_items TO ap_invoice_clerk;