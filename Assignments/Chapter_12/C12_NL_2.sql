use cf;
CREATE OR REPLACE VIEW labor_pricing AS 
SELECT service_id,
       service_date,
       service_desc,
       labor_hours,
       labor_cost,
       labor_hours * labor_cost AS labor_price
FROM service;
SELECT * FROM labor_pricing;
       
         