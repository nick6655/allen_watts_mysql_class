# Allen_Watts_MySQL_Class

This project will create a combo of docker containers to tackle mysql needs for Allen Watts' MySQL class.

### Prerequisites:
*  Mac OSX or Linux Operating System
*  Docker Installed and Running on System (https://www.docker.com/products/docker-desktop)

### Contents:
* Dockerfile -- This is used to customize the mysql image from DockerHub.
* create_databases.sql -- This is a .sql file that is used to build the example databases.
* mysql_class.yml -- DockerCompose file used to bring up the containers.
* run.sh -- Coordinates the creation of the containers and the provisioning of the dbs.
* setup.sh -- Provisions the databases on the db container.

### Usage:
1.  Confirm prerequisites have been met.
2.  Download the contents of this repo and place them in a directory of your choosing.
3.  Use the run script. `./run.sh`
4.  Open a web browser and navigate to http://localhost:8080
    >  USERNAME: root
       PASSWORD: example

### Roadmap:
*  Support for Windows 10