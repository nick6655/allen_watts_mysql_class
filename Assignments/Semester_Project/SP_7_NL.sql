DROP PROCEDURE IF EXISTS get_prod_notes;
DELIMITER //
CREATE PROCEDURE get_prod_notes(
    IN  stock_name_param  VARCHAR(60),
    IN  action_name_param VARCHAR(60),
    OUT prod_notes_param  VARCHAR(6000)
)
BEGIN

    DECLARE ret_note   VARCHAR(6000);

    SELECT notes
    INTO ret_note
    FROM production_notes pn
    INNER JOIN stock_action_fitment saf ON saf.stock_action_id = pn.stock_action_id
    INNER JOIN stocks s ON s.stock_id = saf.stock_id
    INNER JOIN actions a ON a.action_id = saf.action_id
    WHERE UPPER(stock_name)  = UPPER(stock_name_param) AND 
          UPPER(action_name) = UPPER(action_name_param);
    

    IF ret_note = '' THEN
        SET prod_notes_param = 'No notes available.';
    ELSE
        SET prod_notes_param = ret_note;
    END IF;

END //
DELIMITER ;

call mc_stocks.get_prod_notes('A5 SK', 'SAKO 85', @prod_notes_param);
select @prod_notes_param;