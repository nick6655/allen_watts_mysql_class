USE ap;
SELECT CONCAT(vendor_city, ', ', vendor_state) AS location,
       CONCAT(vendor_contact_first_name, ' ', vendor_contact_last_name) AS contact
FROM vendors
ORDER BY vendor_contact_last_name;