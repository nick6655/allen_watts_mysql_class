USE ap;
SELECT v.vendor_id,
       invoice_number,
       invoice_date,
       invoice_total
FROM invoices i
INNER JOIN vendors v ON i.vendor_id = v.vendor_id
WHERE v.vendor_state = 'CA'
ORDER BY v.vendor_id, invoice_date;