DROP DATABASE IF EXISTS mc_stocks;
CREATE DATABASE mc_stocks;
USE mc_stocks;

CREATE TABLE stocks(
    stock_id   INT         NOT NULL AUTO_INCREMENT,
    stock_name VARCHAR(60) NOT NULL,
    CONSTRAINT stock_id_pk PRIMARY KEY (stock_id)
);
CREATE TABLE actions(
    action_id   INT          NOT NULL AUTO_INCREMENT,
    action_name VARCHAR(120) NOT NULL,
    CONSTRAINT action_id_pk PRIMARY KEY (action_id)
);
CREATE TABLE stock_action_fitment(
    stock_action_id       INT       NOT NULL AUTO_INCREMENT,
    stock_id              INT       NOT NULL,
    action_id             INT       NOT NULL,
    drop_at_comb          FLOAT,
    drop_at_heel          FLOAT,
    drop_to_toe           FLOAT,
    forearm_width         FLOAT,
    grip_to_tip           FLOAT,
    butt_width            FLOAT,
    butt_flat             FLOAT,
    grip_width            FLOAT,
    over_all_length       FLOAT,
    length_of_pull_no_pad FLOAT,
    max_action_diameter   FLOAT,
    max_barrel_diameter   FLOAT,
    barrel_contour        VARCHAR(30),
    cast_off              VARCHAR(30),
    toe_angle_in_degrees  VARCHAR(30),
    CONSTRAINT stock_action_id_pk              PRIMARY KEY (stock_action_id),
    CONSTRAINT stock_action_fitment_fk_stocks  FOREIGN KEY (stock_id)  REFERENCES stocks(stock_id),
    CONSTRAINT stock_action_fitment_fk_actions FOREIGN KEY (action_id) REFERENCES actions(action_id)
);
CREATE TABLE production_notes(
    production_note_id      INT   NOT NULL AUTO_INCREMENT,
    stock_action_id         INT   NOT NULL,
    notes                   VARCHAR(6000),
    min_lead_time_in_months INT,
    max_lead_time_in_months INT,
    CONSTRAINT production_note_id_pk                    PRIMARY KEY (production_note_id),
    CONSTRAINT production_notes_fk_stock_action_fitment FOREIGN KEY (stock_action_id) REFERENCES stock_action_fitment(stock_action_id)
);