CREATE OR REPLACE VIEW get_avg_max_lead_time_for_A5_stocks AS
SELECT stock_name, AVG(min_lead_time_in_months) AS avg_min_lead_time, AVG(max_lead_time_in_months) AS avg_max_lead_time
FROM stock_action_fitment sa
INNER JOIN stocks s ON s.stock_id = sa.stock_id
INNER JOIN production_notes pn ON pn.stock_action_id = sa.stock_action_id
WHERE stock_name LIKE "%A5%" AND max_lead_time_in_months <= 6
GROUP BY stock_name;

USE mc_stocks;
SELECT * FROM get_avg_max_lead_time_for_A5_stocks;