USE ap;
SELECT vendor_city,
       vendor_state,
       SUM(invoice_total) AS invoice_totals
FROM vendors
NATURAL JOIN invoices
WHERE vendor_state = 'CA' OR
      vendor_state = 'TN'
GROUP BY vendor_city , vendor_state WITH ROLLUP;